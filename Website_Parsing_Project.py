from selenium import webdriver
#import pandas as pd
#import numpy as np
#import bs4 as bs
import time
from tkinter import *
import csv

#Declare PATH for Chrome WebDriver
PATH = "C:\Program Files (x86)\chromedriver.exe"
#Establish URL
url = 'https://www.barchart.com/stocks/quotes/$SPX/put-call-ratios'
#Load the browser
browser = driver = webdriver.Chrome(PATH)
browser.get(url)
time.sleep(5)
#Use selenium method to find html element <tbody>
search = browser.find_element_by_tag_name("tbody")

#Get the Web Elements and split into lists
body = search.text
data = body.split('\n')
expiration_date = data[0:len(data)-1:9]
dte = data[1:len(data)-1:9]
put_volume = data[2:len(data)-1:9]
call_volume = data[3:len(data)-1:9]
put_call_volume_ratio = data[4:len(data)-1:9]
put_OI = data[5:len(data)-1:9]
call_OI = data[6:len(data)-1:9]
put_call_OI_ratio = data[7:len(data)-1:9]
avg_atm_volatility = data[8:len(data)-1:9]

#Kill the browser
browser.quit()

#Save the Data to Text File
datatable = [expiration_date, dte, put_volume, call_volume, put_call_volume_ratio, put_OI, call_OI, put_call_OI_ratio, avg_atm_volatility]
with open('myfile.txt', 'w') as csvfile:
    mywriter = csv.writer(csvfile, delimiter='|',lineterminator='&',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    mywriter.writerow(datatable)

#Create the GUI Window and set a title, icon and page color
window = Tk()
window.title(" Luke Bach - Webpage Parsing Project")
iconFile = PhotoImage(file = 'LBB_SEM.png')
window.iconphoto(False, iconFile)
window.configure(bg='#000842')

#Create the table Headers and populate with text
Label(window, text = "Expiration Date", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 1, padx = 5, pady =5)
Label(window, text = "DTE", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 2, padx = 5, pady =5)
Label(window, text = "Put Volume", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 3, padx = 5, pady =5)
Label(window, text = "Call Volume", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 4, padx = 5, pady =5)
Label(window, text = "Put/Call Vol Ratio", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 5, padx = 5, pady =5)
Label(window, text = "Put OI", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 6, padx = 5, pady =5)
Label(window, text = "Call OI", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 7, padx = 5, pady =5)
Label(window, text = "Put/Call OI Ratio", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 8, padx = 5, pady =5)
Label(window, text = "Avg ATM Volatility", font = "Bold", fg = '#5cff90', bg='#000842').grid(row = 1, column = 9, padx = 5, pady =5)

#Create the table data columns
for x in range(2, 41):
    Label(window, text = expiration_date[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 1)
for x in range(2, 41):
    Label(window, text = dte[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 2)
for x in range(2, 41):
    Label(window, text = put_volume[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 3)
for x in range(2, 41):
    Label(window, text = call_volume[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 4)
for x in range(2, 41):
    Label(window, text = put_call_volume_ratio[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 5)
for x in range(2, 41):
    Label(window, text = put_OI[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 6)
for x in range(2, 41):
    Label(window, text = call_OI[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 7)
for x in range(2, 41):
    Label(window, text = put_call_OI_ratio[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 8)
for x in range(2, 41):
    Label(window, text = avg_atm_volatility[x-1], fg = '#5cff90', bg='#000842').grid(row = x, column = 9)

#Create the GUI
window.mainloop()
